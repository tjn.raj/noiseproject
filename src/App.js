import React from 'react';

import FormUI from './components/FormUI';
import HistoryUI from './components/HistoryUI';
import Rgpd from './components/Rgpd';

import AppBar from '@material-ui/core/AppBar';
import CameraIcon from '@material-ui/icons/PhotoCamera';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';


import Container from '@material-ui/core/Container';


import './App.css';

const useStyles = makeStyles((theme) => ({
    icon: {
        marginRight: theme.spacing(2),
    },
    content: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),
    },
}));

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
            City Sounds
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
        );
    }

export default function App() {
    const classes = useStyles();

    return (
        <React.Fragment>
        <CssBaseline />
        <AppBar position="relative">
            <Toolbar>
                <CameraIcon className={classes.icon} />
                <Typography variant="h6" color="inherit" noWrap>
                City Sounds
                </Typography>
            </Toolbar>
        </AppBar>

        <main style={{ padding: 15 }}>
            <div className={classes.content}>
                <Container maxWidth="sm">
                    <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                    City Sounds
                    </Typography>
                    <Typography variant="h5" align="center" color="textSecondary" paragraph>
                    Represent Noise effects in a picture :-o 
                    </Typography>
                </Container>

                <FormUI />

                <HistoryUI/>
            </div>
        </main>

        {/* Footer */}
        <footer className={classes.footer}>
            <Typography variant="h6" align="center" gutterBottom>
            Footer
            </Typography>
            <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
            Something here to give the footer a purpose!
            </Typography>
                <Rgpd></Rgpd>
            <Copyright />
        </footer>
        {/* End footer */}
        </React.Fragment>
    );
}
