import React from 'react';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

import axios from 'axios';


import { makeStyles } from '@material-ui/core/styles';
import { IconButton } from '@material-ui/core';
import RefreshIcon from '@material-ui/icons/Refresh';

class HistoryUI extends React.Component {

  
  constructor(props){
    super(props);
    this.intervalID = 0;

    this.state = {
      isloading: true,
      videos: [],
      error: null,
    }
  };

  componentDidMount() {
    this.getHisto().then(() => {
        this.setState({isloading: false});

    })
  }


  componentWillUnmount() {
    /*
      stop getData() from continuing to run even
      after unmounting this component
    */
    clearTimeout(this.intervalID);
  }

  async getHisto() {
    try {
      const respons = await axios.get('http://3.92.78.170/api/noises');
      const result = [];
      for (var i=respons.data.length - 1; i > respons.data.length - 11; i--){
        respons.data[i].video = respons.data[i].video.replace('./noises', 'http://3.92.78.170');
        result.push(respons.data[i])
      }

      this.setState({videos: result})
      this.intervalID = setTimeout(this.getHisto.bind(this), 30000);

    } catch (error) {
      console.error(error);
      this.setState({error: error});
    }
  }

  refreshPage() {
    window.location.reload(false);
  }

  classes = makeStyles((theme) => ({
    cardGrid: {
      paddingTop: theme.spacing(8),
      paddingBottom: theme.spacing(8),
    },
    card: {
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
    details: {
      display: "flex",
      flexDirection: "column",
      marginTop: 16
    },
    cardMedia: {
      paddingTop: '56.25%', // 16:9
    },
    cardContent: {
      flexGrow: 1,
    },
  }));

  render() {
    const {isloading, videos, error} = this.state;

    return (
      <div>
        <Grid item xs={8}  sm={8}>
          <Typography variant="h3" color="textSecondary" paragraph>
              Result of experiences : 
            <IconButton aria-label="delete" onClick={this.refreshPage}>
              <RefreshIcon fontSize="inherit" />
            </IconButton>
          </Typography>
        </Grid>

        {!isloading ? (
          <Container className={this.classes.cardGrid} maxWidth="md">
            <Grid container spacing={4}>
              {videos.map((video) => (
                <Grid item key={video.video + video._id} xs={12} sm={6} md={4}>
                  <Card className={this.classes.card}>
                  <div className={this.classes.details}>
                    <iframe
                        id="video"
                        title={video.video.name + video.email}
                        src="http://3.92.78.170/videos/sample_video.mp4"
                        // src={video.video}
                        frameBorder="0"
                        allow="accelerometer, autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowFullScreen
                    />
                    <Typography align="center" variant="subtitle1" color="textSecondary">
                        {video.email}
                    </Typography>
                    </div>
                  </Card>
                </Grid>
              ))}
            </Grid>
          </Container>
        ) : (
          <Typography color="textSecondary" paragraph>History is loading ...</Typography>
        )}
        {error ? <p>{error.message}</p> : null}
      </div>
    );
  }
}

export default HistoryUI;