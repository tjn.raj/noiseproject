import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';

const styles = (theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(2),
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
});

const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
        <Typography variant="h6">{children}</Typography>
        {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
            <CloseIcon />
        </IconButton>
        ) : null}
    </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function Rgpd() {
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
    setOpen(true);
    };
    const handleClose = () => {
    setOpen(false);
    };

    return (
    <div align="center">
        <Button variant="outlined" color="primary" onClick={handleClickOpen} >
        RGPD informations
        </Button>
        <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
        City sounds
        </DialogTitle>
        <DialogContent dividers>
            <h3>Politique de protection des données personnelles :</h3>
            <Typography gutterBottom>
            <strong>Le projet CITY SOUNDS respecte la réglementation française et européenne sur la protection des données
personnelles, en particulier le Règlement (UE) général sur la protection des données du 27 avril 2016
(“RGPD”) et la Loi Informatique et Libertés du 6 janvier 1978 modifiée (“LIL”).</strong></Typography>
            
            <h3>LES DROITS DES UTILISATEURS</h3>
            
            <Typography gutterBottom>
            Chaque fois qu’on traite des Données Personnelles, on prend toutes les mesures raisonnables pour s’assurer de
l’exactitude et de la pertinence des Données Personnelles au regard des finalités pour lesquelles CITY SOUNDS les
traite.
            </Typography>
            <Typography gutterBottom>
            Conformément à la réglementation européenne en vigueur, les Utilisateurs de CITY SOUNDS disposent des droits
suivants :
            </Typography>
                <ul>    
                    <li>droit d'accès (article 15 RGPD) et de rectification (article 16 RGPD), de mise à jour, de complétude des
données des Utilisateurs</li>
                    <li>droit de verrouillage ou d’effacement des données des Utilisateurs à caractère personnel (article 17 du
RGPD), lorsqu’elles sont inexactes, incomplètes, équivoques, périmées, ou dont la collecte, l'utilisation, la
communication ou la conservation est interdite</li>
                    <li>droit de retirer à tout moment un consentement (article 13-2c RGPD)</li>
                    <li>droit à la limitation du traitement des données des Utilisateurs (article 18 RGPD)</li>
                    <li>droit d’opposition au traitement des données des Utilisateurs (article 21 RGPD)</li>
                    <li>droit à la portabilité des données que les Utilisateurs auront fournies, lorsque ces données font l’objet de
traitements automatisés fondés sur leur consentement ou sur un contrat (article 20 RGPD)</li>
                    <li>droit de définir le sort des données des Utilisateurs après leur mort et de choisir à qui CITY SOUNDS devra
communiquer (ou non) ses données à un tiers qu’ils aura préalablement désigné</li>
                    <li>Milk</li>
                </ul>

            <Typography gutterBottom>
            Si l’Utilisateur souhaite savoir comment CITY SOUNDS utilise ses Données Personnelles, demander à les rectifier
ou s’oppose à leur traitement, l’Utilisateur peut contacter CITY SOUNDS par mail à taoufiq.germoud@epita.fr. Dans
ce cas, l’Utilisateur doit indiquer les Données Personnelles qu’il souhaiterait que CITY SOUNDS corrige, mette à jour
ou supprime, en s’identifiant de manière précise avec une copie d’une pièce d’identité (carte d’identité ou passeport)
ou tout autre élément permettant de justifier de son identité. Les demandes de suppression de Données Personnelles
seront soumises aux obligations qui sont imposées à CITY SOUNDS par la loi, notamment en matière de conservation
ou d’archivage des documents. Enfin, les Utilisateurs de CITY SOUNDS peuvent déposer une réclamation auprès
des autorités de contrôle, et notamment de la CNIL (https://www.cnil.fr/fr/plaintes).
            </Typography>

        </DialogContent>
        <DialogActions>
            <Button autoFocus onClick={handleClose} color="primary">
            I understand
            </Button>
        </DialogActions>
        </Dialog>
    </div>
);
}
