import React from 'react';
import axios from 'axios';

// MATERIAL
import {
    Paper,
    Grid,
    TextField
} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';

import { makeStyles } from '@material-ui/core/styles';

// ICONS
import IconButton from '@material-ui/core/IconButton';
import PlayArrow from '@material-ui/icons/PlayArrow';
import Stop from '@material-ui/icons/Stop';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Icon from '@material-ui/core/Icon';

import MicRecorder from 'mic-recorder-to-mp3';


const Mp3Recorder = new MicRecorder({ bitRate: 128 });


class FormUI extends React.Component {

    constructor(props){
        super(props);
    
        this.state = {
            name: '',
            // image
            selectedImg: null,
            imgDisplay: null,
            //record
            isRecording: false,
            blobURL: '',
            isBlocked: false,
            file: '',
        };

        this.errors = {
            name: true,
            myphoto: true,
            myaudio: true,
            all: true,
        }

        this.handleChange = this.handleChange.bind(this);
    };

    componentDidMount() {
        navigator.getUserMedia({ audio: true },
        () => {
            console.log('Permission Granted');
            this.setState({ isBlocked: false });
        },
        () => {
            console.log('Permission Denied');
            this.setState({ isBlocked: true })
        },
        );
    }


    // update name
    handleChange(event) {
        this.setState({name: event.target.value});
        this.errors.name = !event.target.value? true : false;
        this.validate()
    }

    // select image
    imgSelectHandler = event => {
        if (event.target.files && event.target.files[0]) {
            if (event.target.files[0].type === "image/jpeg") {
                this.setState({selectedImg: event.target.files[0]})
                
                let reader = new FileReader();
                reader.onload = (e) => {
                    this.setState({imgDisplay: e.target.result});
                };
                reader.readAsDataURL(event.target.files[0]);

                this.validate();
            } else {
                alert('Need jpeg type');
                console.error("Need jpeg");
            }
        }
    };


    // RECORD

  // record part
    start = () => {
        if (this.state.isBlocked) {
        console.log('Permission Denied');
        } else {
        Mp3Recorder
            .start()
            .then(() => {
            this.setState({ isRecording: true });
            }).catch((e) => console.error(e));
        }
    };

    stop = () => {
        Mp3Recorder
        .stop()
        .getMp3()
        .then(([buffer, blob]) => {
            const file = new File(buffer, "me-at-thevoice.mp3", {
            type: blob.type,
            lastModified: Date.now()
            });
            const blobURL = URL.createObjectURL(blob)
            this.setState({ blobURL: blobURL, isRecording: false });
            this.setState({ file: file });
            this.validate()
        }).catch((e) => console.log(e));
    };

    // RESET 
    reset() {
        this.setState({
            name: '',
            selectedImg: null,
            imgDisplay: null,
            isRecording: false,
            blobURL: '',
            isBlocked: false,
            file: '',
        });
        this.validate()
    }

    
    validate() {
        this.errors.name = !this.state.name ? true : false;
        this.errors.myphoto = !this.state.selectedImg ? true : false;
        this.errors.myaudio = !this.state.file ? true : false;
        
        this.errors.all = !this.errors.name && !this.errors.myphoto && !this.errors.myaudio 
        ? false : true;
    };
    
    
    // SUBMIT
    handleSubmit = async values => {
        if (!this.errors.all) {
            var fd = new FormData();
            const headers = {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': 'true',
            }
    
            fd.append('email', this.state.name);
            fd.append('myphoto', this.state.selectedImg);
            fd.append('myaudio', this.state.file);
    
            axios.post('http://3.92.78.170/api/noises', fd, headers, {
            onUploadProgress: processEvent => {
                console.log('Upload Progress: ' + Math.round(processEvent.loaded / processEvent.total  * 100) + '%');
            }}).then(res => {
                alert('Your project is send :)');
            });
        } else {
            alert('Please filled all information (name, image and record)');
        }
    }



    classes = makeStyles((theme) => ({
        root: {
            '& > *': {
                margin: theme.spacing(1),
            },
        },
        input: {
            display: 'none',
        },
        button: {
            margin: theme.spacing(1),
        },
        media: {
            height: 0,
            paddingTop: '56.25%', // 16:9
        },
    }));

    render() {

        return (
            <div>
            <Typography variant="h3" color="textSecondary" paragraph>
            Try it yourself :) 
            </Typography>
            <Container style={{ padding: 15 }}>
                <Paper style={{ padding: 16 }}>
                <Grid container alignItems="flex-start" spacing={2}>
                    {/* name */}
                    <Grid item xs={12}>                       

                    {this.errors.name ? 
                        <TextField 
                            error 
                            id="name" 
                            required
                            label="Your city sounds name"
                            type="text"
                            fullWidth
                            value={this.state.name}
                            onChange={this.handleChange}
                        />
                    :
                        <TextField 
                            id="name" 
                            required
                            label="Your city sounds name"
                            type="text"
                            fullWidth
                            helperText="Required"
                            value={this.state.name}
                            onChange={this.handleChange}
                            />
                        }

                    </Grid>

                    {/* image */}
                    <Grid item style={{ marginTop: 16 }} xs={12}>
                        {!this.state.imgDisplay ? 
                            <Typography color="error" paragraph>
                                Image is missing*
                            </Typography> :
                            <img 
                                id="target"
                                alt="Your city sounds intact."
                                src={this.state.imgDisplay} 
                            />
                        }
                    </Grid>
                            
                    <Grid item xs={12}>

                        <Button
                            type="button"
                            variant="contained"
                            onClick={() => this.fileInput.click()}
                            color="primary"
                            component="span"
                            style={{ marginRight: 16 }}
                            className={this.classes.button}
                            startIcon={<CloudUploadIcon />}
                        >
                            Upload Image
                        </Button>

                        <input
                            name="myphoto"
                            required
                            id="contained-button-file"
                            className={this.classes.input}
                            type="file"
                            accept="image/*"
                            style={{display: 'none'}}
                            onChange={this.imgSelectHandler} 
                            ref={fileInput => this.fileInput = fileInput}
                        />
                    </Grid>


                    {/* RECORD */}
                    <Grid item={true} xs={12}  sm={12} style={{ marginTop: 16 }} >

                    </Grid>
                    <Grid item={true} xs={2}  sm={2} >
                        <Typography color="textSecondary" paragraph>Recording :</Typography>
                        
                        <IconButton 
                            color="primary"
                            type="button"
                            // variant="contained"
                            onClick={this.start} 
                            disabled={this.state.isRecording}

                            >
                            <PlayArrow />
                        </IconButton>
                        
                        <IconButton 
                            color="primary"
                            type="button"
                            // variant="contained"
                            onClick={this.stop} 
                            disabled={!this.state.isRecording}
                        >
                            <Stop />
                        </IconButton>


                    </Grid>

                    <Grid item={true} xs={10}  sm={10} style={{ marginTop: 16 }} >
                        <audio src={this.state.blobURL} controls="controls" />
                        {this.errors.myaudio ? 
                            <Typography color="error" paragraph>
                                Record is missing
                            </Typography> :
                            <Typography color="textPrimary" paragraph >
                                Record is saving
                            </Typography>
                        }
                    </Grid>

                    {/* RESET */}
                    {/* <Grid item style={{ marginTop: 16 }}>
                    <Button
                        type="button"
                        variant="contained"
                        onClick={this.reset}
                        // disabled={submitting || pristine}
                    >
                        Reset
                    </Button>
                    </Grid> */}


                    {/* SUBMIT */}
                    <Grid item style={{ marginTop: 16 }}>
                    <Button
                        variant="contained"
                        color="primary"
                        type="button"
                        // onClick={this.handleSubmit}
                        onClick={this.handleSubmit}
                        disabled={!this.errors.all}
                        className={this.classes.button}
                        endIcon={<Icon>send</Icon>}
                    >
                        Submit
                    </Button>
                    {/* {this.errors.all ? 
                        <Typography color="error" paragraph>
                            Please select all value needed before submit
                        </Typography> :
                        <Typography color="textPrimary" paragraph >
                            
                        </Typography>
                    }
                        */}
                        </Grid>
                    </Grid>
                </Paper>
            </Container>
        </div>
        );
    };
}

export default FormUI;
